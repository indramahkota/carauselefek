﻿package {
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;

	import themes.CustomTheme;

	import views.CarouViews;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Main extends Sprite {
		private var stgWidth:Number;
		private var stgHeight:Number;

		public function Main() {
			new CustomTheme();

			stgWidth = Starling.current.stage.stageWidth;
			stgHeight = Starling.current.stage.stageHeight;

			/* var main:Sprite3D = new Sprite3D();
			main.x = stgWidth / 2;
			main.y = stgHeight / 2;
			this.addChild(main);

			var ukuran:Number = (stgWidth % 8) == 0 ? stgWidth : Math.ceil(stgWidth / 8) * 8;  
			
			var m:Matrix = new Matrix();
			m.createGradientBox(ukuran, ukuran, 0, 0, 0);

			// var color:uint = Math.random() * 0xffffff;
			// trace(uint2hex(color));

			var fS:flash.display.Sprite = new flash.display.Sprite();
			fS.graphics.beginFill(0x02283F);
			fS.graphics.drawRect(0, 0, ukuran, ukuran);
			fS.graphics.endFill();
			
			var checkers:BitmapData = new BitmapData(ukuran, ukuran, true, 0x0);
			checkers.draw(fS);

			var num:Number = Math.ceil(stgWidth / 8);

			for (var yP:int = 0; yP < num; yP++)
			{
				for (var xP:int = 0; xP < num; xP++)
				{
					if ((yP + xP) % 2 == 0)
					{
						checkers.fillRect(new Rectangle(xP * 8, yP * 8, 8, 8), 0x0);
					}
				}
			}
			
			var checkerTx:Texture = Texture.fromBitmapData(checkers);
			
			var backImg:Image = new Image(checkerTx);
			backImg.alignPivot();

			var bottomImg:Image = new Image(checkerTx);
			bottomImg.alignPivot();

			var leftImg:Image = new Image(checkerTx);
			leftImg.alignPivot();

			var rightImg:Image = new Image(checkerTx);
			rightImg.alignPivot();

			var back:Sprite3D = new Sprite3D();
			back.addChild(backImg);
			back.rotationX = Math.PI;
			back.z = ukuran / 2;
			main.addChild(back);

			var bottom:Sprite3D = new Sprite3D();
			bottom.addChild(bottomImg);
			bottom.y = ukuran / 2;
			bottom.rotationX = Math.PI / 2.0;
			main.addChild(bottom);

			var left:Sprite3D = new Sprite3D();
			left.addChild(leftImg);
			left.x = -1 * ukuran / 2;
			left.rotationY = Math.PI / 2.0;
			main.addChild(left);

			var right:Sprite3D = new Sprite3D();
			right.addChild(rightImg);
			right.x = ukuran / 2;
			right.rotationY = Math.PI / -2.0;
			main.addChild(right);

			// var quad:Quad = new Quad(ukuran, ukuran, 0x00ffff);
			// quad.alpha = 0.5;
			// quad.alignPivot();

			var quad:Image = new Image(Texture.fromColor(ukuran, ukuran, 0x00ffff, .5));
			quad.alignPivot();

			var bottom2:Sprite3D = new Sprite3D();
			bottom2.addChild(quad);
			bottom2.y = ukuran / 4;
			bottom2.rotationX = Math.PI / 2.0;
			main.addChild(bottom2);

			var waveFilter:WaveFilter = new WaveFilter();
            var linear_source:WaveSource = new WaveSource(WaveSource.LINEAR, .01, .5, 10, 10);
            var radial_source:WaveSource = new WaveSource(WaveSource.RADIAL, .05, 5, 10, 10, new Point(.5,1), 1);
            
            waveFilter.addWaveSource(linear_source);
            waveFilter.addWaveSource(radial_source);
            quad.filter = waveFilter;		
            Starling.juggler.add(waveFilter); */

			var test:CarouViews = new CarouViews(stgWidth / 2, stgHeight / 2);
			this.addChild(test);

			addEventListener(Event.ENTER_FRAME, function(e:Event):void {
				test.update();
			});
		}

		private function uint2hex(dec:uint):String {
			var digits:String = "0123456789ABCDEF";
			var hex:String = '';
		
			while (dec > 0) {
				var next:uint = dec & 0xF;
				dec >>= 4;
				hex = digits.charAt(next) + hex;
			}
		
			if (hex.length == 0) hex = '0'
		
			return hex;
		}
	}
}