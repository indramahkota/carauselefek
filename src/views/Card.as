package views {
    import feathers.controls.Label;

    import starling.display.MeshBatch;
    import starling.display.Sprite;

    import utilities.RoundedQuad;

    /**
	 * ...
	 * @author Indra Mahkota
	 */
    public class Card extends Sprite {
        private var _id:int;
        private var _color:uint;
        private var _cwidth:Number;
        private var _cheight:Number;

        public function Card(id:int, width:Number, height:Number, color:uint) {
            _id = id;
            _color = color;
            _cwidth = width;
            _cheight = height;

            setupCard();
        }

        private function setupCard():void {
            var mb:MeshBatch = new MeshBatch();
            this.addChild(mb);

            var bg:RoundedQuad = new RoundedQuad(10, _cwidth, _cheight, _color);
            bg.alignPivot();
            mb.addMesh(bg);

            var bg1:RoundedQuad = new RoundedQuad(10, _cwidth, _cheight, _color);
            bg1.alignPivot();
            bg1.rotation = Math.PI / 4;
            mb.addMesh(bg1);
            
            var txt:Label = new Label();
            txt.text = String(_id + 1);
            txt.validate();
            txt.alignPivot();
            this.addChild(txt);
        }
    }
}