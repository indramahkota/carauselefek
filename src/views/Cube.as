package views {
    import flash.display.BitmapData;
    import flash.display.GradientType;
    import flash.geom.Matrix;

    import starling.display.Image;
    import starling.display.Sprite3D;
    import starling.textures.Texture;

    /**
	 * ...
	 * @author Indra Mahkota
	 */
    public class Cube extends Sprite3D {
        private var ukuran:Number = 50;

        public function Cube(cl1:uint, cl2:uint) {
			// var bottomImg:Quad = new Quad(ukuran, ukuran, Math.random() * 0xffffff);
            // bottomImg.alignPivot();

			var bottomImg:Image = new Image(Texture.fromColor(ukuran, ukuran, Math.random() * 0xffffff));
			bottomImg.alignPivot();

			var bottom:Sprite3D = new Sprite3D();
			bottom.addChild(bottomImg);
			bottom.y = ukuran / 2;
			bottom.rotationX = Math.PI / 2.0;
			this.addChild(bottom);

			// var quad:Quad = new Quad(5, 100, Math.random() * 0xffffff);
			// quad.alignPivot(Align.CENTER, Align.TOP);

			var quad:Image = new Image(Texture.fromColor(5, 100, 0x8152B1));
			quad.alignPivot();

			var flat:Sprite3D = new Sprite3D();
			flat.addChild(quad);
			flat.y = ukuran / 2;
			this.addChild(flat);

			var m:Matrix = new Matrix();
			m.createGradientBox(ukuran, ukuran, 0, 0, 0);

			// var color:uint = Math.random() * 0xffffff;
			// trace(uint2hex(color));

			var fS:flash.display.Sprite = new flash.display.Sprite();
			fS.graphics.beginGradientFill(GradientType.RADIAL, [/* Math.random() * 0xffffff */0xB8BB8E, /* Math.random() * 0xffffff */0x8152B1], [1, 1], [0, 255], m); //.beginFill(Math.random() * 0xffffff);
			fS.graphics.drawCircle(25, 25, 25);
			fS.graphics.endFill();
			
			var checkers:BitmapData = new BitmapData(ukuran, ukuran, true, 0x0);
			checkers.draw(fS);

			var circle:Image = new Image(Texture.fromBitmapData(checkers));
			circle.alignPivot();

			var circle3d:Sprite3D = new Sprite3D();
			circle3d.addChild(circle);
			circle3d.y = (ukuran / 2) + 50;
			this.addChild(circle3d);

            // var frontImg:Quad = new Quad(ukuran, ukuran, Math.random() * 0xffffff);
            // frontImg.alignPivot();

			var frontImg:Image = new Image(Texture.fromColor(ukuran, ukuran, Math.random() * 0xffffff));
			frontImg.alignPivot();

            var front:Sprite3D = new Sprite3D();
            front.addChild(frontImg);
            front.z = -ukuran / 2;
            this.addChild(front);

            // var backImg:Quad = new Quad(ukuran, ukuran, Math.random() * 0xffffff);
            // backImg.alignPivot();

			var backImg:Image = new Image(Texture.fromColor(ukuran, ukuran, Math.random() * 0xffffff));
			backImg.alignPivot();

            var back:Sprite3D = new Sprite3D();
			back.addChild(backImg);
			back.rotationX = Math.PI;
			back.z = ukuran / 2;
			this.addChild(back);

            // var topImg:Quad = new Quad(ukuran, ukuran, Math.random() * 0xffffff);
            // topImg.alignPivot();

			var topImg:Image = new Image(Texture.fromColor(ukuran, ukuran, Math.random() * 0xffffff));
			topImg.alignPivot();

			var top:Sprite3D = new Sprite3D();
			top.addChild(topImg);
			top.y = -ukuran / 2;
			top.rotationX = Math.PI / -2.0;
			this.addChild(top);

            // var leftImg:Quad = new Quad(ukuran, ukuran, Math.random() * 0xffffff);
            // leftImg.alignPivot();

			var leftImg:Image = new Image(Texture.fromColor(ukuran, ukuran, Math.random() * 0xffffff));
			leftImg.alignPivot();

			var left:Sprite3D = new Sprite3D();
			left.addChild(leftImg);
			left.x = -1 * ukuran / 2;
			left.rotationY = Math.PI / 2.0;
			this.addChild(left);

            // var rightImg:Quad = new Quad(ukuran, ukuran, Math.random() * 0xffffff);
            // rightImg.alignPivot();

			var rightImg:Image = new Image(Texture.fromColor(ukuran, ukuran, Math.random() * 0xffffff));
			rightImg.alignPivot();

			var right:Sprite3D = new Sprite3D();
			right.addChild(rightImg);
			right.x = ukuran / 2;
			right.rotationY = Math.PI / -2.0;
			this.addChild(right);

			
        }
    }
}