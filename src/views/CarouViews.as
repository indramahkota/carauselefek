package views {
	import flash.display3D.Context3DTriangleFace;
	import flash.geom.Matrix3D;
	import flash.geom.Point;

	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.display.Sprite3D;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.rendering.Painter;
	import starling.utils.Color;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class CarouViews extends Sprite {
		private var objs:Array;

		private var stgWidth:Number;
		private var stgHeight:Number;

        private var main:Sprite3D;
        private var wrap:Sprite3D;

		private var movement:Number = 0.05;
		private var mouseDown:Boolean = false;

		private var mainfocus:Quad;

		public function CarouViews(possX:Number = 0, possY:Number = 0) {
			stgWidth = Starling.current.stage.stageWidth;
			stgHeight = Starling.current.stage.stageHeight;

			main = new Sprite3D();
			main.x = possX;
			main.y = possY;
			main.name = "main";
			main.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			this.addChild(main);

			wrap = new Sprite3D();
			wrap.y = -50;
			// wrap.z = 400;
			main.addChild(wrap);

            mainfocus = new Quad(10, 10, Color.GREEN);
            mainfocus.name = "mainfocus";
			mainfocus.alignPivot();
			mainfocus.x = possX;
			mainfocus.y = possY;
            mainfocus.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			this.addChild(mainfocus);

            var wrapfocus:Quad = new Quad(10, 10, Color.YELLOW);
            wrapfocus.name = "wrapfocus";
			wrapfocus.alignPivot();
            wrapfocus.addEventListener(TouchEvent.TOUCH, onTouchHandler);
            wrapfocus.y = -50;
			main.addChild(wrapfocus);

			var cl1:uint = Math.random() * 0xffffff;
			var cl2:uint = Math.random() * 0xffffff;

			trace(uint2hex(cl1), uint2hex(cl2));

			objs = [];
			for(var i:int = 0; i < 15; i++) {
				//var qd:Card = new Card(i, 100, 100, Math.random() * 0xffffff);

				var sp:Sprite3D = new Sprite3D();
				sp.x = 175 * Math.sin(i * 360 / 15 * Math.PI / 180);
				sp.z = 175 * Math.cos(i * 360 / 15 * Math.PI / 180);
				sp.alignPivot();
				sp.addChild(new Cube(cl1, cl2));
				
				wrap.addChild(sp);
				objs.push(sp);
			}
		}

		private function uint2hex(dec:uint):String {
			var digits:String = "0123456789ABCDEF";
			var hex:String = '';
		
			while (dec > 0) {
				var next:uint = dec & 0xF;
				dec >>= 4;
				hex = digits.charAt(next) + hex;
			}
		
			if (hex.length == 0) hex = '0'
		
			return hex;
		}

		public function update():void {
			var currentDate:Date = new Date();
			// main.y = mainfocus.y + (Math.cos(currentDate.getTime() * 0.002) * 20);				

			if(!mouseDown) {
                wrap.rotationY -= movement;
                if(Math.abs(movement) > 0.05)
               		movement *= .90;
            }

			var arr:Array = [];
			for(var i:int = 0; i < objs.length; i++) {
				var ele:Sprite3D = objs[i] as Sprite3D;
				ele.rotationY = -wrap.rotationY;

				var quad:Sprite3D = ele.getChildAt(0) as Sprite3D;
				// quad.rotationX += .05;
				// quad.rotationY += .05;
				// quad.rotationZ += .05;

				var quad1:Sprite3D = quad.getChildAt(2) as Sprite3D;
				quad1.y = quad1.y + (Math.cos(currentDate.getTime() * 0.002));

				var mtx:Matrix3D = ele.getTransformationMatrix3D(main);
				arr.push({ele:ele, z:mtx.position.z});
			}
			
			arr.sortOn("z", Array.NUMERIC | Array.DESCENDING);
			
			var base:Number = wrap.z;
			
			for(i=0; i<arr.length; i++) {
				ele = arr[i].ele as Sprite3D;
				
				var z:Number = arr[i].z;
				wrap.setChildIndex(ele, i);
				
				var b:Number = Math.abs(z);
				
				b = z / 10;

				// ele.filter = (b > 2) ? new BlurFilter(b, b, 6) : null;
				// ele.visible = (b > 50) ? false : true ;
			}
		}

        private function onTouchHandler(event:TouchEvent):void {
            var target:DisplayObject = event.target as DisplayObject;
            var touch:Touch = event.getTouch(this);

            if (touch) {
                if(touch.phase == TouchPhase.BEGAN) {
					mouseDown = true;
				} else if(touch.phase == TouchPhase.MOVED) {
                    if(target.name == "mainfocus") {
                        main.pivotX = -1 * (touch.getLocation(this).x - stgWidth/2);
                        main.pivotY = -1 * (touch.getLocation(this).y - stgHeight/2);
						target.x = touch.getLocation(this).x;
						target.y = touch.getLocation(this).y;
                    } else if(target.name == "wrapfocus"){
                        wrap.x = touch.getLocation(this).x - main.x;
                        wrap.y = touch.getLocation(this).y - main.y;
						target.x = touch.getLocation(this).x - main.x;
                    	target.y = touch.getLocation(this).y - main.y;
                    } else {
						var move:Point = touch.getMovement(this);
						movement = move.x * .01;
						wrap.rotationY -= movement;
					}

                } else if(touch.phase == TouchPhase.ENDED) {
					mouseDown = false;
				}
            }
        }

		override public function render(painter:Painter):void
		{
			painter.pushState();
            painter.state.culling = Context3DTriangleFace.BACK;
            super.render(painter);
            painter.popState();
		}
	}
}