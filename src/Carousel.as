package {
	import feathers.utils.ScreenDensityScaleFactorManager;

	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3DProfile;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;

	import starling.core.Starling;
	
	[SWF(width = "380", height = "680", frameRate = "60", backgroundColor = "#FFFFFF")]
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Carousel extends Sprite {
		private var starling:Starling;
		private var scaler:ScreenDensityScaleFactorManager;
		
		public function Carousel() {
			if(this.stage) {
				this.stage.scaleMode = StageScaleMode.NO_SCALE;
				this.stage.align = StageAlign.TOP_LEFT;
			}

			this.mouseEnabled = this.mouseChildren = false;

			starling = new Starling(Main, this.stage, null, null, Context3DRenderMode.AUTO, Context3DProfile.BASELINE);
			starling.skipUnchangedFrames = true;
			starling.antiAliasing = 4;
			starling.showStatsAt();

			this.stage.addEventListener(flash.events.Event.DEACTIVATE, stage_deactivateHandler, false, 0, true);
			scaler = new ScreenDensityScaleFactorManager(starling);
			starling.start();
		}
		
		private function stage_deactivateHandler(event:flash.events.Event):void {
			starling.stop(true);
			this.stage.addEventListener(flash.events.Event.ACTIVATE, stage_activateHandler, false, 0, true);
		}
		
		private function stage_activateHandler(event:flash.events.Event):void {
			this.stage.removeEventListener(flash.events.Event.ACTIVATE, stage_activateHandler);
			starling.start();
		}
	}
}