﻿package themes {
	import feathers.controls.Label;
	import feathers.controls.text.StageTextTextEditor;
	import feathers.controls.text.TextBlockTextRenderer;
	import feathers.core.FeathersControl;
	import feathers.core.ITextEditor;
	import feathers.core.ITextRenderer;
	import feathers.themes.StyleNameFunctionTheme;

	import starling.text.TextFormat;
		
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class CustomTheme extends StyleNameFunctionTheme {
		[Embed(source = "/../fonts/SourceSansPro-Regular.ttf", fontFamily = "SourceSansPro", mimeType = "application/x-font", embedAsCFF = "true")]
		protected static const SourceSansPro_Regular:Class;
		
		protected static const fontName:String = "SourceSansPro";
		
		protected var normalLabelFontStyles:TextFormat;

		public function CustomTheme() {
			super();
			initialize();
		}
		
		//-------------------------
		// Disposes the atlas before calling super.dispose()
		//-------------------------
		
		override public function dispose():void {
			super.dispose();
		}
		
		//-------------------------
		// Initializes the theme
		//-------------------------
		
		protected function initialize():void {
			initializeFonts();
			initializeGlobals();
			initializeStyleProviders();
		}
		
		//-------------------------
		// Initializes fonts
		//-------------------------
		
		protected function initializeFonts():void {
			normalLabelFontStyles = new TextFormat(fontName, 45);
		}
		
		//-------------------------
		// Initializes global variables
		//-------------------------
		
		protected function initializeGlobals():void {
			FeathersControl.defaultTextRendererFactory = textRendererFactory;
			FeathersControl.defaultTextEditorFactory = textEditorFactory;
		}
		
		//defaultTextRendererFactory
		private static function textRendererFactory():ITextRenderer {
			return new TextBlockTextRenderer();
		}
		
		//defaultTextEditorFactory
		private static function textEditorFactory():ITextEditor {
			return new StageTextTextEditor();
		}
		
		//-------------------------
		// Sets global style providers for all components
		//-------------------------		
		
		protected function initializeStyleProviders():void {
			getStyleProviderForClass(Label).defaultStyleFunction = setLabelStyles;
		}

		protected function setLabelStyles(label:Label):void {
			label.fontStyles = normalLabelFontStyles;
		}
	}
}