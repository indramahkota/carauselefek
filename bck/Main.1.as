﻿// package
// {
// 	import flash.display3D.Context3DTriangleFace;
// 	import flash.geom.Matrix3D;

// 	import starling.core.Starling;
// 	import starling.display.Quad;
// 	import starling.display.Sprite;
// 	import starling.display.Sprite3D;
// 	import starling.events.Event;
// 	import starling.rendering.Painter;
	
// 	/**
// 	 * ...
// 	 * @author Indra Mahkota
// 	 */
// 	public class Main extends Sprite
// 	{
// 		private var mouseX:Number = 0;
// 		private var mouseY:Number = 0;

// 		public function Main()
// 		{
// 			// addEventListener(TouchEvent.TOUCH, onTouch);

// 			var stgWidth:Number = Starling.current.stage.stageWidth;
// 			var stgHeight:Number = Starling.current.stage.stageHeight;

// 			var imageSize:Number;
            
//             if(stgHeight > stgWidth) {
//                imageSize = 2 * Math.sqrt(Math.pow((stgHeight / 2), 2) + Math.pow((stgWidth / 2), 2));
//             } else {
//                 imageSize = 2 * Math.sqrt(Math.pow((stgWidth / 2), 2) + Math.pow((stgWidth / 2), 2));
//             }

// 			var gb:Quad = new Quad(stgWidth, stgHeight);
// 			gb.setVertexColor(0, Math.random() * 0xffffff);
// 			gb.setVertexColor(1, Math.random() * 0xffffff);
// 			gb.setVertexColor(2, Math.random() * 0xffffff);
// 			gb.setVertexColor(3, Math.random() * 0xffffff);
// 			this.addChild(gb);

// 			/* var m:Matrix = new Matrix();
// 			m.createGradientBox(imageSize, imageSize);

// 			// Create gradient backgroun
// 			var fS:flash.display.Sprite = new flash.display.Sprite();
// 			fS.graphics.beginGradientFill(GradientType.RADIAL, [Math.random() * 0xffffff, Math.random() * 0xffffff], [1, 1], [0, 255], m);
// 			// fS.graphics.drawRect(0, 0, imageSize, imageSize);
// 			fS.graphics.drawCircle(stgWidth, stgWidth, stgWidth);
// 			fS.graphics.endFill();
			
// 			// Draw the gradient to the bitmap data
// 			var checkers:BitmapData = new BitmapData(imageSize, imageSize, true, 0x0);
// 			checkers.draw(fS);

// 			for (var yP:int = 0; yP < 160; yP++)
// 			{
// 				for (var xP:int = 0; xP < 160; xP++)
// 				{
// 					if ((yP + xP) % 2 == 0)
// 					{
// 						checkers.fillRect(new Rectangle(xP * 32, yP * 32, 32, 32), 0xffffff);
// 					}
// 				}
// 			}
			
// 			var checkerTx:Texture = Texture.fromBitmapData(checkers);
// 			checkers.dispose();
			
// 			var background:Image = new Image(checkerTx);
//             // background.blendMode = "none";
// 			background.alignPivot();
// 			background.x = stgWidth / 2;
// 			background.y = stgHeight / 2;
// 			// this.addChild(background);

// 			var spd:Sprite3D = new Sprite3D();
// 			spd.rotationX = deg2rad(90);
// 			spd.addChild(background);			
// 			this.addChild(spd); */

// 			var main:Sprite3D = new Sprite3D();
// 			main.x  = stgWidth / 2;
// 			main.y = stgHeight / 2;
// 			this.addChild(main);

// 			var wrap:Sprite3D = new Sprite3D();
// 			wrap.y = -100;
// 			wrap.z = 50;
// 			main.addChild(wrap);

// 			var wrap2:Sprite3D = new Sprite3D();
// 			wrap2.y = 100;
// 			wrap2.z = 50;
// 			main.addChild(wrap2);

// 			var objs:Array = [];
// 			var objs2:Array = [];
// 			for(var i:int = 0; i < 15; i++) {
// 				var qd:Quad = new Quad(50, 50, Math.random() * 0xffffff);
// 				qd.alignPivot();

// 				var qd2:Quad = new Quad(50, 50, Math.random() * 0xffffff);
// 				qd2.alignPivot();

// 				// var qd:Canvas = new Canvas();
// 				// qd.beginFill(Math.random() * 0xffffff);
// 				// qd.drawCircle(25, 25, 25);
// 				// qd.alignPivot();
// 				// qd.endFill();

// 				var sp:Sprite3D = new Sprite3D();
// 				sp.x = 120 * Math.sin(i * 360 / 15 * Math.PI / 180);
// 				sp.z = 120 * Math.cos(i *360 / 15 * Math.PI / 180);
// 				sp.addChild(qd);

// 				var sp2:Sprite3D = new Sprite3D();
// 				sp2.x = 120 * Math.sin(i * 360 / 15 * Math.PI / 180);
// 				sp2.z = 120 * Math.cos(i *360 / 15 * Math.PI / 180);
// 				sp2.addChild(qd2);

// 				wrap.addChild(sp);
// 				wrap2.addChild(sp2);

// 				objs.push(sp);
// 				objs2.push(sp2);
// 			}

// 			/* var waveFilterCircle:WaveFilter = new WaveFilter();
//             var linear_source:WaveSource = new WaveSource(WaveSource.LINEAR, .01, .5, 10, 10);
//             var radial_source:WaveSource = new WaveSource(WaveSource.RADIAL, .02, 5, 10, 10, new Point(.5,1), 1);
            
//             waveFilterCircle.addWaveSource(linear_source);
//             waveFilterCircle.addWaveSource(radial_source);
            
// 			background.filter = waveFilterCircle;

//             Starling.juggler.add(waveFilterCircle); */

// 			var _cube:Sprite3D = createCube(100);
//             _cube.z = 100;
// 			main.addChild(_cube);

// 			// var godRays:GodRayPlane = new GodRayPlane(stgWidth + 200, stgHeight / 2);
// 			// godRays.x = -100;
//             // godRays.color = 0xf;
//             // godRays.speed = 0.1;
//             // godRays.size = 0.1;
//             // godRays.skew = 0.25;
//             // godRays.shear = 0;
//             // godRays.fade = 1;
//             // godRays.contrast = 0.5;
            
//             // addChild(godRays);
//             // Starling.juggler.add(godRays);

// 			// if(Transitions.getTransition(Animations.SHAKE_3X) == null)
//             //         Animations.registerTransitions();
                
// 			// var getar:Tween = new Tween(wrap, 1);
// 			// getar.transition = Animations.SHAKE_3X;
// 			// getar.repeatCount = 100;
// 			// getar.animate("x", wrap.x + 5);
// 			// getar.onComplete = function():void {
// 			// 	Starling.juggler.remove(getar);
// 			// }
// 			// Starling.juggler.add(getar);

// 			Starling.current.stage.addEventListener(Event.ENTER_FRAME, function (e:Event):void {				
// 				// wrap.rotationY -= (mouseX / stgWidth * 480 - wrap.rotationY) * 0.5;
// 				wrap.rotationY -= 0.02;
// 				wrap2.rotationY += 0.02;
// 				// background.rotation += 0.01;				
				
// 				var arr:Array = [];
// 				var arr2:Array = [];
// 				for(var i:int = 0; i<objs.length; i++) {
// 					var ele:Sprite3D = objs[i] as Sprite3D;
// 					ele.rotationY = -wrap.rotationY;

// 					var ele2:Sprite3D = objs2[i] as Sprite3D;
// 					ele2.rotationY = -wrap2.rotationY;
					
// 					var mtx:Matrix3D = ele.getTransformationMatrix3D(main);
// 					arr.push({ele:ele, z:mtx.position.z});

// 					var mtx2:Matrix3D = ele2.getTransformationMatrix3D(main);
// 					arr2.push({ele:ele2, z:mtx2.position.z});
// 				}
				
// 				arr.sortOn("z", Array.NUMERIC | Array.DESCENDING);
// 				arr2.sortOn("z", Array.NUMERIC | Array.DESCENDING);
				
// 				var base:Number = wrap.z;
// 				var base2:Number = wrap.z;
				
// 				for(i=0; i<arr.length; i++) {
// 					ele = arr[i].ele as Sprite3D;
// 					ele2 = arr2[i].ele as Sprite3D;
					
// 					var z:Number = arr[i].z;
// 					wrap.setChildIndex(ele, i);
					
// 					var b:Number = Math.abs(z);
					
// 					b = z / 10;

// 					var z2:Number = arr2[i].z;
// 					wrap2.setChildIndex(ele2, i);
					
// 					var b2:Number = Math.abs(z2);
					
// 					b = z / 10;
// 					b2 = z2 / 10;
					
// 					// ele.filter = (b > 2) ? new BlurFilter(b, b, 6) : null;
// 					// ele.visible = (b > 2) ? false : true ;
// 				}

// 				_cube.rotationX += 0.02;
// 				_cube.rotationY += 0.02;
// 				_cube.rotationZ += 0.02;

// 				setRequiresRedraw();
// 			});

			
// 		}

// 		private function createCube(_width:Number):Sprite3D
//         {
//             var offset:Number = _width / 2;
            
//             var front:Sprite3D = createSidewall(offset);
//             front.z = -offset;
            
//             var back:Sprite3D = createSidewall(offset);
//             back.rotationX = Math.PI;
//             back.z = offset;
            
//             var top:Sprite3D = createSidewall(offset);
//             top.y = - offset;
//             top.rotationX = Math.PI / -2.0;
            
//             var bottom:Sprite3D = createSidewall(offset);
//             bottom.y = offset;
//             bottom.rotationX = Math.PI / 2.0;
            
//             var left:Sprite3D = createSidewall(offset);
//             left.x = -offset;
//             left.rotationY = Math.PI / 2.0;
            
//             var right:Sprite3D = createSidewall(offset);
//             right.x = offset;
//             right.rotationY = Math.PI / -2.0;
            
//             var cube:Sprite3D = new Sprite3D();
//             cube.addChild(front);
//             cube.addChild(back);
//             cube.addChild(top);
//             cube.addChild(bottom);
//             cube.addChild(left);
//             cube.addChild(right);
			
//             return cube;
//         }

// 		private function createSidewall(offset:Number):Sprite3D
//         {
//             var image:Quad = new Quad(offset * 2, offset * 2, Math.random() * 0xffffff);
//             image.alignPivot();
            
//             var sprite:Sprite3D = new Sprite3D();
//             sprite.addChild(image);
//             return sprite;
//         }

// 		// private function onTouch(e:TouchEvent):void {
// 		// 	var touch:Touch = e.getTouch(this);

//         //     if(touch){
//         //         if (touch.phase == TouchPhase.HOVER ) {
// 		// 			var location:Point = touch.getLocation(this);
// 		// 			mouseX = location.x;
// 		// 			mouseY = location.y;
// 		// 		}
// 		// 	}
// 		// }

// 		override public function render(painter:Painter):void
// 		{
// 			painter.pushState();
//             painter.state.culling = Context3DTriangleFace.BACK;
//             super.render(painter);
//             painter.popState();
// 		}
// 	}
// }